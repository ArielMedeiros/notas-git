# Notas sobre o GIT

Criação do projeto:
	
	$ git init

Adição de arquivo para ser monitado:

	$ git add arquivo

Obter informações sobre o projeto:

	$ git status

Criação de uma nova versão:

	$ git commit -m "Descrição da nova versão"

Para listar as versões:
	
	$git log (--oneline, --decorate)

Para alterar a versão atual:

	$ git checkout versão 

Para enviar alterações para um servidor remoto:

    $ git push origin master
    
Para contribuir com um projeto você precisar forkar. Enviar modificações para sua cópia e criar um pull request. 